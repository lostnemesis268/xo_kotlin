package org.kotlinlang.play.Introduction
fun main(){
    printWelcome()
    while (true) {
        printMenu()
        val selectMenu = readLine()
        if (selectMenu == "2") {
            break
        } else if (selectMenu == "1") {
            var player: Char = 'o'
            val table = arrayOf(
                charArrayOf('-','-','-'),
                charArrayOf('-','-','-'),
                charArrayOf('-','-','-'))
            while (true) {
                try {
                    printTable(table)
                    println("Player $player turn")
                    print("Please select row and col:")
                    val input = readLine()
                    val numStar = input?.split(" ")
                    if(numStar?.size ?:0 == 2) {
                        val row = (numStar?.get(0)?.toInt())!! -1
                        val col = (numStar?.get(1)?.toInt())!! -1
                        if(checkRowCol(row, col)){
                            if (table[row!!][col!!] == '-'){
                                table[row][col] = player
                                player = changePlayer(player)
                                println("You selected "+(row+1)+", "+(col+1))
                                if(checkWin(table)){
                                    println()
                                    break
                                }
                            } else {
                                println("This channel is selected!!")
                            }

                        } else {
                            println("Please select 1-3 for row and col!!")
                        }
                    } else {
                        println("Please select row and col! [Ex1. 3 1 || 3 is row ,1 is col]")
                    }
                } catch (e:NumberFormatException){
                    println("Please input number!!")
                }
            }
        }
        else {
            println("Please select 1-3!!!\n")
        }
    }
    printThankyou()
}


private fun checkWin(table: Array<CharArray>): Boolean {
    for (p in 0..1) {
        var currentPlayer: Char = 'o'
        if (p == 0) currentPlayer = 'o'
        else currentPlayer = 'x'
        for (x in 0..2) {
            if (table[x][0] == currentPlayer
                && table[x][1] == currentPlayer
                && table[x][2] == currentPlayer) {
                printTable(table)
                println("$currentPlayer is Winner!")
                return true
            }

        }
        for (x in 0..2) {
            if (table[0][x] == currentPlayer
                && table[1][x] == currentPlayer
                && table[2][x] == currentPlayer) {
                printTable(table)
                println("$currentPlayer is Winner!")
                return true
            }

        }
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer){
            printTable(table)
            println("$currentPlayer is Winner!")
            return true
        } else if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer){
            printTable(table)
            println("$currentPlayer is Winner!")
            return true
        }
    }
    return false
}

private fun changePlayer(player: Char): Char {
    var player1 = player
    if (player1 == 'o') player1 = 'x'
    else player1 = 'o'
    return player1
}

private fun checkRowCol(row: Int?, col: Int?) = (row ?:0 <= 2 && row ?:0 >= 0
        && col ?:0 <= 2 && col ?:0 >= 0)

private fun printTable(table :Array<CharArray>) {
    println()
    for (row in table){
        for (col in row){
            print("$col ")
        }
        println()
    }
}

private fun printWelcome() {
    println("\tWelcome to XO game!")
}

private fun printMenu() {
    println("Menu")
    println("1.Play")
    println("2.Exit")
    print("Please select menu: ")
}

private fun printThankyou() {
    println("\n\t______________________")
    println("\tThank you for playing!")
    println("\t______________________")
}
